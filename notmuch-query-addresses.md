% notmuch-query-addresses(1) Notmuch-tools


# NAME

notmuch-query-addresses - query for addresses in a notmuch database

# SYNOPSIS

**notmuch-query-addresses** [*option*...] *pattern*...

# DESCRIPTION

The **notmuch-query-addresses** command extracts email addresses from
the messages indexed in a **notmuch**(1) database. The *pattern* is
searched for both in the email addresses or in the associated names.

The results are written on standard output as one address per line, by
default in the standard format `Name <address>`. When using the option
**-m**, the format is instead that of Mutt’s address query system.

Addresses are extracted from the standard **From**, **To****, ****Cc**,
**Bcc** and **Reply-To** headers of indexed messages. They are stored in
a cache database which is updated when the command is called with the
**-u** option, so that in normal usage each message is only scanned
once. The initial scan of a big database of messages can take some time,
though.

# OPTIONS

**-h**, **--help**
: Display a description of the command-line arguments and exit.

**-c**, **--cache**=FILE
: Set the name of the cache file. The default name is
  `notmuch/addresses.db` in the user’s cache folder (`$XDG_CACHE_HOME`
  if defined, otherwise `$HOME/.cache`).

**-m**, **--mutt**
: Use an output format suitable for use with Mutt as a
  `query_command`: the first line is blank and each line afterwards
  contains three fields separated by tabs, respectively the email
  address, the associated name and a comment that provides the date of
  the last message where this address appears. If no name is
  associated with the email, then the comment is not shown

**-q**, **--quiet**
: Do not display progress information when scanning the message
  database during an update.

**-u**, **--update**
: Update the address cache before querying. When this option is used,
  the query argument is optional. When **-u** is used without a query,
  the database is updated but no query is performed.

# ENVIRONMENT

**NOTMUCH_CONFIG**
: The location of the notmuch configuration file. By default, the path
  is `$HOME/.notmuch-config` as for **notmuch**(1). This path to the
  mail store is read from this file.

# AUTHOR

Emmanuel Beffara <manu@beffara.org>

# SEE ALSO

**notmuch**(1)
