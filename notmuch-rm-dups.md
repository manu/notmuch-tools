% notmuch-rm-dups(1) Notmuch-tools


# NAME

notmuch-rm-dups - remove duplicate messages in a notmuch database

# SYNOPSIS

**notmuch-rm-dups** [*option*...] [*search-term*...]

# DESCRIPTION

The **notmuch-rm-dups** command removes duplicate message files for
messages indexed in a **notmuch**(1) database. The search terms
indicates which messages will be examined. For each message, only the
first file (as reported by notmuch) is preserved, the remaining files
are deleted.

Note that notmuch treats messages as identical as soon as they have the
same `Message-ID`, and **notmuch-rm-dups** does not try to do better.
Therefore, this operation may lose data if two messages with the same
`Message-ID` have actually different contents.

# OPTIONS

**-h**, **--help**
: Display a description of the command-line arguments and exit.

**-n**, **--dry-run**
: Simulate a move but do not actually change the local database.

**-v**, **--verbose**
: Display operations before doing them.

# ENVIRONMENT

**NOTMUCH_CONFIG**
: The location of the notmuch configuration file. By default, the path
  is `$HOME/.notmuch-config` as for **notmuch**(1).

# AUTHOR

Emmanuel Beffara <manu@beffara.org>

# SEE ALSO

**notmuch**(1)
