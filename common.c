#include <locale.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <curses.h>
#include <term.h>

#include <glib.h>
#include <gio/gio.h>
#include <notmuch.h>

#include "common.h"

/* Errors specific to notmuch-tools */

GQuark
nt_error_quark (void)
{
	return g_quark_from_static_string ("local-error-quark");
}


/* Notmuch errors, the Glib way */

GQuark
notmuch_error_quark (void)
{
	return g_quark_from_static_string ("notmuch-error-quark");
}

gboolean
notmuch_check_status (notmuch_status_t status, GError **error)
{
	if (status == NOTMUCH_STATUS_SUCCESS)
		return TRUE;
	g_set_error(error, NOTMUCH_ERROR, status,
			"Notmuch error: %s", notmuch_status_to_string(status));
	return FALSE;
}


/* General configuration variables */

gboolean conf_dry_run = FALSE;
gboolean conf_quiet   = FALSE;
gboolean conf_verbose = FALSE;


/* Progress reports. */

GCond progress_condition;
GMutex progress_mutex;
GThread *progress_thread;
gint64 progress_interval;
gboolean progress_finished;

const char * progress_phase = "";
unsigned int progress_count = 0;
unsigned int progress_total = 0;

static gboolean progress_with_curses;
static char * seq_rewind;
static char * seq_clear;
GPrintFunc saved_print_handler;

static void
progress_bar ()
{
	int width, num, count;

	if (progress_total == 0) {
		fputs("...", stdout);
		return;
	}

	/* Compute the size of the count. */

	count = 1;
	for (num = progress_total; num > 10; num /= 10) count++;

	/* Deduce the available width for the progress bar:
	 * <text> [=====   ] <count>/<total> */

	width = tigetnum("cols");
	if (width < 0) width = 80;
	width -= strlen(progress_phase) + 2 * count + 6 ;
	if (width < 0) return;
	if (width > 40) width = 40;

	/* Render the bar. */

	num = progress_count * width / progress_total;
	if (num < 0) num = 0;
	else if (num > width) num = width;
	width -= num;

	putchar('[');
	for (; num > 0; num--) putchar('=');
	for (; width > 0; width--) putchar(' ');
	printf("] %d/%d", progress_count, progress_total);
}

static gpointer
progress_loop (gpointer data)
{
	gint64 next_time;

	g_mutex_lock(&(progress_mutex));
	next_time = g_get_monotonic_time() + progress_interval;
	for (;;)
		if (g_cond_wait_until(&(progress_condition), &(progress_mutex), next_time)) {
			if (progress_finished) break;
		} else {
			printf("%s%s ", seq_rewind, progress_phase);
			progress_bar();
			fputs(seq_clear, stdout);
			fflush(stdout);
			next_time += progress_interval;
		}
	g_mutex_unlock(&(progress_mutex));
	fputs(seq_rewind, stdout);
	fputs(seq_clear, stdout);

	return NULL;
}

static void
progress_print (const char *string)
{
	fputs(seq_rewind, stdout);
	for (; *string != 0; string++) {
		if (*string == '\n') {
			fputs(seq_clear, stdout);
			break;
		} else {
			putchar(*string);
		}
	}
	fputs(string, stdout);
	fflush(stdout);
}

void
progress_start (gint64 interval)
{
	int err;

	/* Do no start progress reports if they are already started.*/
	if (progress_thread != NULL) return;

	/* Only start progress reports when writing to a terminal, then query
	 * capabilities for updating the messages. */
	if (!isatty(STDOUT_FILENO)) return;
	progress_with_curses = (setupterm(NULL, 1, &err) == OK);
	if (progress_with_curses) {
		seq_clear = tigetstr("el");
		if (seq_clear == NULL) seq_clear = " ";
		seq_rewind = tiparm(tigetstr("hpa"), 0);
		if (seq_rewind == NULL) seq_rewind = "\r";
	} else {
		seq_clear = " ";
		seq_clear = "\r";
	}

	progress_interval = interval;
	progress_finished = FALSE;
	saved_print_handler = g_set_print_handler(progress_print);
	g_mutex_lock(&(progress_mutex));
	progress_thread = g_thread_new("progress", progress_loop, NULL);
	g_mutex_unlock(&(progress_mutex));
}

void
progress_stop ()
{
	if (progress_thread == NULL) return;
	g_mutex_lock(&(progress_mutex));
	progress_finished = TRUE;
	g_cond_signal(&(progress_condition));
	g_mutex_unlock(&(progress_mutex));
	g_thread_join(progress_thread);
	progress_thread = NULL;
	g_set_print_handler(saved_print_handler);
}

/**
 * main:
 * @argc: the argument count
 * @argv: the argument table
 *
 * The main entry point of the program. This performs basinc initialization,
 * parses the command line with the function parse_commandline() and calls the
 * main program in main_with_error().
 *
 * Returns: 0 on successful run, 1 on failure.
 */

int
main (int argc, char *argv[])
{
	GError *error = NULL;

	setlocale(LC_CTYPE, "");

#if !GLIB_CHECK_VERSION(2, 35, 0)
	g_type_init();
#endif

	if ( !parse_commandline(&argc, &argv, &error)
	  || !main_with_error(&error)
	) {
		g_printerr("%s\n", error->message);
		return 1;
	}

	return 0;
}
