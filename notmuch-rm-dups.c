#include <glib.h>
#include <gio/gio.h>
#include <notmuch.h>

#include "common.h"

/* Configuration variables. */

GString * conf_query_string   = NULL;

/* Command line. */

static const char *parameter_string =
"[TERMS...] - delete message files in a notmuch database";

static const char *summary =
"Positional arguments:\n"
"  TERMS            Search terms for the set of messages";

static const GOptionEntry entries[] =
{
	{ "dry-run", 'n', 0, G_OPTION_ARG_NONE, &conf_dry_run,
		"Do not perform operations", NULL },
	{ "verbose", 'v', 0, G_OPTION_ARG_NONE, &conf_verbose,
		"Show operations before doing them", NULL },
	{ NULL }
};

gboolean
parse_commandline (int *argc, char ***argv, GError **error)
{
	gboolean status = FALSE;

	GOptionContext *context = g_option_context_new(parameter_string);
	g_option_context_set_summary(context, summary);
	g_option_context_add_main_entries(context, entries, NULL);

	if (!g_option_context_parse(context, argc, argv, error)) goto end;

	conf_query_string = g_string_new("");
	if (*argc < 2)
		g_string_assign(conf_query_string, "*");
	else for (int i = 1; i < *argc; i++) {
		if (conf_query_string->len > 0)
			g_string_append_c(conf_query_string, ' ');
		g_string_append(conf_query_string, (*argv)[i]);
	}

	status = TRUE;
end:
	g_option_context_free(context);
	return status;
}

/**
 * delete_dups_message:
 * @db: the Notmuch database holding the message (must be writeable)
 * @message: a Notmuch message to move
 * @error: a #GError
 *
 * Delete and unindex duplicate files for a message. Only one file is kept.
 *
 * Returns: %TRUE on success.
 */

static gboolean
delete_dups_message (
	notmuch_database_t *db,
	notmuch_message_t *message,
	GError **error)
{
	const char *filename;
	char *first_name = NULL;
	GFile *file = NULL;
	gboolean result = FALSE;
	int index = 0;
	notmuch_filenames_t *filenames;
	notmuch_status_t status;

	for (
			filenames = notmuch_message_get_filenames(message);
			notmuch_filenames_valid(filenames);
			notmuch_filenames_move_to_next(filenames)) {
		filename = notmuch_filenames_get(filenames);

		/* Skip the file if it is the first one. */

		switch (index++) {
		case 0:
			first_name = g_strdup(filename);
			continue;
		case 1:
			if (conf_verbose)
				g_print("message: %s\n   file: %s\n",
						notmuch_message_get_message_id(message), first_name);
		default:
			if (conf_verbose)
				g_print("    dup: %s\n", filename);
		}

		if (conf_dry_run)
			continue;

		/* Unindex the file. The status from notmuch should not be
		 * NOTMUCH_STATUS_SUCCESS because this would mean that we removed the
		 * last file for the given message. The actual success code here is
		 * NOTMUCH_STATUS_DUPLICATE_MESSAGE_ID. */
		
		status = notmuch_database_remove_message(db, filename);
		g_assert(status != NOTMUCH_STATUS_SUCCESS);
		if (status != NOTMUCH_STATUS_DUPLICATE_MESSAGE_ID) {
			g_assert(!notmuch_check_status(status, error));
			goto end;
		}

		/* Remove the file. */

		file = g_file_new_for_path(filename);
		if (!g_file_delete(file, NULL, error))
			goto end;
		g_clear_object(&file);
	}

	result = TRUE;
end:
	g_free(first_name);
	g_clear_object(&file);
	return result;
}

/*
 * Main entry point.
 */

gboolean
main_with_error (GError **error)
{
	gboolean result = FALSE;
	notmuch_database_t *db = NULL;
	notmuch_query_t *query = NULL;
	notmuch_messages_t *messages;

	NOTMUCH(database_open_with_config, NULL,
		conf_dry_run ? NOTMUCH_DATABASE_MODE_READ_ONLY
		: NOTMUCH_DATABASE_MODE_READ_WRITE,
		NULL, NULL, &db, NULL);

	query = notmuch_query_create(db, conf_query_string->str);
	NOTMUCH(query_search_messages, query, &messages);
	for (;
		notmuch_messages_valid(messages);
		notmuch_messages_move_to_next(messages)
	) {
		notmuch_message_t *message = notmuch_messages_get(messages);
		gboolean status = delete_dups_message(db, message, error);
		notmuch_message_destroy(message);
		if (!status) return FALSE;
	}

	result = TRUE;
end:
	if (query) notmuch_query_destroy(query);
	if (db) notmuch_database_close(db);
	return result;
}
