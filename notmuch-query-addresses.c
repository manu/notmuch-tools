#include <glib.h>
#include <gio/gio.h>
#include <gmime/gmime.h>
#include <notmuch.h>
#include <sqlite3.h>

#include "common.h"
#include "sqlite.h"

/* Configuration variables. */

char *    conf_cache_path   = NULL;
GFile *   conf_cache        = NULL;
GString * conf_query_string = NULL;
gboolean  conf_update       = FALSE;
gboolean  conf_mutt         = FALSE;

/* Global state. */

sqlite3 *database;
#define sqlite_db database

/* Command line. */

static const char *parameter_string =
"QUERY... - search for addresses in a notmuch database";

static const char *summary =
"Positional arguments:\n"
"  QUERY            The pattern to search for";

GOptionEntry entries[] =
{
	{ "mutt", 'm', 0, G_OPTION_ARG_NONE, &conf_mutt,
		"Use an output format suitable for Mutt", NULL },
	{ "cache", 'c', 0, G_OPTION_ARG_FILENAME, &conf_cache_path,
		"Filename for the cache database", "FILE" },
	{ "quiet", 'q', 0, G_OPTION_ARG_NONE, &conf_quiet,
		"Suppress progress messages", NULL },
	{ "dump-sql", 's', 0, G_OPTION_ARG_NONE, &conf_dump_sql,
		"Print the SQL queries before doing them", NULL },
	{ "update", 'u', 0, G_OPTION_ARG_NONE, &conf_update,
		"Update the cache (then QUERY is optional)", NULL },
	{ NULL }
};

gboolean parse_commandline (int *argc, char ***argv, GError **error)
{
	GOptionContext *context;
	gboolean status = FALSE;

	context = g_option_context_new(parameter_string);
	g_option_context_set_summary(context, summary);
	g_option_context_add_main_entries(context, entries, NULL);

	if (!g_option_context_parse(context, argc, argv, error)) goto end;

	/* Get the path to the cache database. */

	if (conf_cache_path != NULL)
		conf_cache = g_file_new_for_commandline_arg(conf_cache_path);

	if (conf_cache == NULL) {
		const char *var = g_getenv("XDG_CACHE_HOME");
		if (var != NULL) {
			GFile *file = g_file_new_for_path(var);
			conf_cache = g_file_resolve_relative_path(
				file, "notmuch/addresses.db");
			g_object_unref(file);
		}
	}

	if (conf_cache == NULL) {
		const char *var = g_getenv("HOME");
		if (var != NULL) {
			GFile *file = g_file_new_for_path(var);
			conf_cache = g_file_resolve_relative_path(
				file, ".cache/notmuch/addresses.db");
			g_object_unref(file);
		}
	}

	if (conf_cache == NULL) {
		g_set_error(error, NT_ERROR, NT_ERROR_MISSING_CACHE,
				"Cannot determine the path of the cache file");
		goto end;
	}

	/* Make the query string. */

	if (!conf_update && *argc < 2) {
		g_set_error(error, G_OPTION_ERROR, G_OPTION_ERROR_FAILED,
				"Missing query argument");
		goto end;
	}

	if (*argc >= 2) {
		int i;
		conf_query_string = g_string_new("");
		for (i = 1; i < *argc; i++) {
			if (conf_query_string->len > 0)
				g_string_append_c(conf_query_string, ' ');
			g_string_append(conf_query_string, (*argv)[i]);
		}
	}

	status = TRUE;
end:
	g_option_context_free(context);
	return status;
}

/**
 * add_entry:
 * @name: the name of a contact
 * @email: the e-mail address of the contact
 * @date: the date of the message that contains the reference to the contact
 * @error: a #GError
 *
 * Add an entry to the addess database. If the entry already exists then its
 * date is updated if the value of @date is larger.
 *
 * Returns: %TRUE on success
 */

/* Implementation note: the process here consists in doing
 * - SELECT to get the current date
 * - INSERT if no entry was found by SELECT
 * - UPDATE if some entry was found and it was older than the new date
 *
 * An alternative approach would be
 * - UPDATE with a MAX to update the entry if it is present
 * - INSERT if there was no entry (i.e. the number of changed rows is 0)
 * but it appears that this second method is about 40% slower.
 */

static gboolean
add_entry (const char *name, const char *email, int date, GError **error)
{
	gboolean result = FALSE;
	int status;

	static sqlite3_stmt *stmt_get = NULL;
	static sqlite3_stmt *stmt_insert = NULL;
	static sqlite3_stmt *stmt_update = NULL;

	g_assert(email != NULL);
	if (name == NULL) name = "";

	if (stmt_get == NULL) {
		SQLITE(prepare_v2, database,
			"SELECT last_used FROM addresses WHERE name=? AND email=?",
			-1, &stmt_get, NULL);
	} else {
		sqlite3_reset(stmt_get);
	}

	SQLITE(bind_text, stmt_get, 1, name, -1, SQLITE_STATIC);
	SQLITE(bind_text, stmt_get, 2, email, -1, SQLITE_STATIC);
	if (conf_dump_sql) sqlite_dump(stmt_get);
	status = sqlite3_step(stmt_get);
	switch (status) {
	case SQLITE_DONE:
		/* The entry does not exist yet. */
		if (stmt_insert == NULL) {
			SQLITE(prepare_v2, database,
				"INSERT INTO addresses VALUES (?, ?, ?)",
				-1, &stmt_insert, NULL);
		} else {
			sqlite3_reset(stmt_insert);
		}
		SQLITE(bind_text, stmt_insert, 1, name, -1, SQLITE_STATIC);
		SQLITE(bind_text, stmt_insert, 2, email, -1, SQLITE_STATIC);
		SQLITE(bind_int, stmt_insert, 3, date);
		if (conf_dump_sql) sqlite_dump(stmt_insert);
		status = sqlite3_step(stmt_insert);
		if (status != SQLITE_DONE && !sqlite_check_status(status, error))
			return FALSE;
		break;

	case SQLITE_ROW:
		/* The entry exists, update it if it is older. */
		if (sqlite3_column_int(stmt_get, 0) >= date) break;
		if (stmt_update == NULL) {
			SQLITE(prepare_v2, database,
				"UPDATE addresses SET last_used=? WHERE name=? AND email=?",
				-1, &stmt_update, NULL);
		} else {
			sqlite3_reset(stmt_update);
		}
		SQLITE(bind_int, stmt_update, 1, date);
		SQLITE(bind_text, stmt_update, 2, name, -1, SQLITE_STATIC);
		SQLITE(bind_text, stmt_update, 3, email, -1, SQLITE_STATIC);
		if (conf_dump_sql) sqlite_dump(stmt_update);
		status = sqlite3_step(stmt_update);
		if (status != SQLITE_DONE && !sqlite_check_status(status, error))
			return FALSE;
		break;

	default:
		sqlite_check_status(status, error);
		return FALSE;
	}

	result = TRUE;
end:
	return result;
}

/**
 * handle_message:
 * @message: a #notmuch_message_t
 * @error: a #GError
 *
 * Extract addresses from a message and store them in the database. Basic
 * filtering is applied to remove incorrect addresses possibly produced by
 * badly quoted headers. Addresses are normalised to lowercase.
 *
 * Returns: %TRUE on success
 */

static const char *address_headers[] =
{ "From", "To", "Cc", "Bcc", "Reply-To", NULL };

static gboolean
handle_message (notmuch_message_t *message, GError **error)
{
	const char **header, *name, *email;
	InternetAddressList *list = NULL;
	int date, i, n;
	gboolean result = FALSE;
	static GRegex *regex = NULL;
	static GString *buffer = NULL;

	if (regex == NULL) {
		regex = g_regex_new("^[^[:space:]\"'@]+@[^[:space:]\"'@]+$", 0, 0, error);
		if (regex == NULL) return FALSE;
	}

	if (buffer == NULL)
		buffer = g_string_new("");

	date = notmuch_message_get_date(message);

	for (header = address_headers; *header != NULL; header++) {
		g_clear_object(&list);
		list = internet_address_list_parse(NULL,
			notmuch_message_get_header(message, *header));
		if (list == NULL) continue;

		n = internet_address_list_length(list);
		for (i = 0; i < n; i++) {
			InternetAddress *address =
				internet_address_list_get_address(list, i);
			if (!INTERNET_ADDRESS_IS_MAILBOX(address)) continue;
			g_assert(address != NULL);
			name = internet_address_get_name(address);
			email =	internet_address_mailbox_get_addr(
				INTERNET_ADDRESS_MAILBOX(address));
			if (email == NULL || !g_regex_match(regex, email, 0, NULL))
				continue;
			if (name != NULL && !g_ascii_strcasecmp(name, email))
				name = NULL;
			g_string_assign(buffer, email);
			g_string_ascii_down(buffer);
			if (!add_entry(name, buffer->str, date, error))
				goto end;
		}
	}

	result = TRUE;
end:
	g_clear_object(&list);
	return result;
}

/**
 * update_cache:
 * @error: a #GError
 *
 * Update the address cache with addresses extracted from new messages.
 *
 * Returns: %TRUE on success, %FALSE if some error occurred.
 */

static gboolean
update_cache (GError **error)
{
	notmuch_database_t *db = NULL;
	notmuch_query_t *query = NULL;
	notmuch_messages_t *messages;
	notmuch_message_t *message;
	unsigned long date, last_date, new_date;
	gboolean result = FALSE;
	char *temp;

	/* Create the SQLite tables if needed and begin a transaction. */

	SQL("CREATE TABLE IF NOT EXISTS state (last_date INTEGER)");

	SQL("CREATE TABLE IF NOT EXISTS addresses"
		" (name TEXT, email TEXT, last_used INTEGER)");

	SQL("BEGIN");

	/* Open the Notmuch database and create the query. */

	NOTMUCH(database_open_with_config, NULL,
		NOTMUCH_DATABASE_MODE_READ_ONLY, NULL, NULL, &db, NULL);

	new_date = last_date = 0;
	SQLv("SELECT * FROM state", get_values, 0, G_TYPE_ULONG, &last_date, 0);
	new_date = last_date;

	temp = g_strdup_printf("%lu..", last_date);
	query = notmuch_query_create(db, temp);
	g_free(temp);

	/* Start printing progress reports unless operation is quiet. */

	if (!conf_quiet) {
		NOTMUCH(query_count_messages, query, &progress_total);
		progress_phase = "Parsing messages";
		progress_count = 0;
		progress_start(G_TIME_SPAN_SECOND/2);
	}

	/* Handle each message in the query. */

	NOTMUCH(query_search_messages, query, &messages);
	for (;
		notmuch_messages_valid(messages);
		notmuch_messages_move_to_next(messages)
	) {
		message = notmuch_messages_get(messages);
		if (!handle_message(message, error)) goto end;
		date = (int) notmuch_message_get_date(message);
		if (date > new_date) new_date = date;
		notmuch_message_destroy(message);
		progress_count ++;
	}

	/* Update the last_date in the database and finish the transaction. */

	if (new_date > last_date) {
		SQLv("UPDATE state SET last_date=?",
			NULL, G_TYPE_ULONG, new_date, 0);
		if (sqlite3_changes(database) == 0)
			SQLv("INSERT INTO state VALUES (?)",
				NULL, G_TYPE_ULONG, new_date, 0);
	}

	SQL("COMMIT");
	result = TRUE;
end:
	if (!conf_quiet) progress_stop();
	if (query != NULL) notmuch_query_destroy(query);
	if (db != NULL) notmuch_database_close(db);
	return result;
}

/**
 * show_address:
 * @stmt: a #sqlite3_stmt containing a row of data
 * @args: ignored
 * @error: a #GError
 *
 * Display the contents of a row of data in Mutt's expected format on standard
 * output.
 *
 * Returns: always %TRUE
 */

static gboolean
show_address (sqlite3_stmt *stmt, va_list args, GError **error)
{
	const unsigned char *name = sqlite3_column_text(stmt, 0);

	if (name == NULL || name[0] == 0) {
		/* If there is no name then we must only print the
		 * e-mail address and no comment even when using Mutt format
		 * because Mutt apparently cannot handle entries with an empty
		 * name and a non-empty comment. */
		g_print("%s\n", sqlite3_column_text(stmt, 1));

	} else if (conf_mutt) {
		GDateTime *timestamp =
			g_date_time_new_from_unix_utc(sqlite3_column_int(stmt, 2));
		char *comment =
			g_date_time_format(timestamp, "used %Y-%m-%d %H:%M");

		g_print("%s\t%s\t%s\n", sqlite3_column_text(stmt, 1), name, comment);

		g_free(comment);
		g_date_time_unref(timestamp);

	} else {
		g_print("%s <%s>\n", name, sqlite3_column_text(stmt, 1));
	}

	return TRUE;
}

/*
 * Main entry point.
 */

gboolean
main_with_error (GError **error)
{
	char *temp = NULL;
	gboolean result = FALSE;

	if (conf_update) {
		/* When updating, make sure the folder of the cache file exists. */

		GFile *dir = g_file_get_parent(conf_cache);
		GError *io_error = NULL;
		gboolean result = g_file_make_directory_with_parents(dir, NULL, &io_error);
		g_object_unref(dir);
		if (!result) {
			if (io_error->domain == G_IO_ERROR
			 && io_error->code == G_IO_ERROR_EXISTS) {
				g_error_free(io_error);
			} else {
				g_propagate_error(error, io_error);
				return FALSE;
			}
		}
	}

	temp = g_file_get_path(conf_cache);
	SQLITE(open, temp, &database);

	if (conf_update && !update_cache(error)) goto end;

	if (conf_query_string != NULL) {
		g_free(temp);
		temp = g_strdup_printf("%%%s%%", conf_query_string->str);
		if (conf_mutt)
			g_print("Search results for %s:\n", conf_query_string->str);

		SQLv(
			"SELECT name, email, last_used FROM addresses"
			" WHERE name LIKE ?1 OR email LIKE ?1 ORDER BY last_used DESC",
			show_address,
			G_TYPE_STRING, temp, 0);
	}

	result = TRUE;
end:
	g_free(temp);
	return result;
}
