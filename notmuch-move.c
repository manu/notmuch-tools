#include <glib.h>
#include <gio/gio.h>
#include <notmuch.h>

#include "common.h"

/* Configuration variables. */

GFile *   conf_directory      = NULL;
GString * conf_query_string   = NULL;
char *    conf_folder         = NULL;

/* Command line. */

static const char *parameter_string =
"FOLDER TERMS... - move messages in a notmuch database";

static const char *summary =
"Positional arguments:\n"
"  FOLDER           Destination folder\n"
"  TERMS            Search terms for the set of messages";

static const GOptionEntry entries[] =
{
	{ "dry-run", 'n', 0, G_OPTION_ARG_NONE, &conf_dry_run,
		"Do not perform operations", NULL },
	{ "verbose", 'v', 0, G_OPTION_ARG_NONE, &conf_verbose,
		"Show operations before doing them", NULL },
	{ NULL }
};

gboolean
parse_commandline (int *argc, char ***argv, GError **error)
{
	GOptionContext *context;
	gboolean status = FALSE;
	int i;

	context = g_option_context_new(parameter_string);
	g_option_context_set_summary(context, summary);
	g_option_context_add_main_entries(context, entries, NULL);

	if (!g_option_context_parse(context, argc, argv, error)) goto end;

	if (*argc < 3) {
		g_set_error(error, G_OPTION_ERROR, G_OPTION_ERROR_FAILED,
				"Missing arguments");
		goto end;
	}

	conf_folder = (*argv)[1];

	conf_query_string = g_string_new("");
	for (i = 2; i < *argc; i++) {
		if (conf_query_string->len > 0)
			g_string_append_c(conf_query_string, ' ');
		g_string_append(conf_query_string, (*argv)[i]);
	}

	status = TRUE;
end:
	g_option_context_free(context);
	return status;
}

/**
 * move_message:
 * @db: the Notmuch database holding the message (must be writeable)
 * @message: a Notmuch message to move
 * @directory: the destination directory
 * @error: a #GError
 *
 * Move all files associated to a given message into some directory. The
 * destination directory is typically the "cur" directory of a Maildir folder.
 *
 * Returns: %TRUE on success.
 */

static gboolean
move_message (
		notmuch_database_t *db,
		notmuch_message_t *message,
		GFile *directory,
		GError **error)
{
	gboolean result = TRUE;
	notmuch_status_t status;
	notmuch_filenames_t *filenames;
	static GString *new_name = NULL;

	if (conf_verbose)
		g_print("message: %s\n",
			notmuch_message_get_message_id(message));

	for (
		filenames = notmuch_message_get_filenames(message);
		result && notmuch_filenames_valid(filenames);
		result ? notmuch_filenames_move_to_next(filenames) : (void)0
	) {
		const char *filename = notmuch_filenames_get(filenames);
		GFile *file = g_file_new_for_path(filename);
		GFile *dir = g_file_get_parent(file);
		GFile *destination = NULL;

		/* Check that the file is not already in the destination
		 * folder. */

		if (g_file_equal(dir, directory))
			goto end_file;

		/* Create a Maildir-compliant name for the destination file. */

		if (new_name == NULL)
			new_name = g_string_new("");
		gint64 time = g_get_real_time();
		g_string_printf(new_name,
			"%" G_GINT64_FORMAT ".M%" G_GINT64_FORMAT
			"R%" G_GUINT32_FORMAT ".%s",
			time / 1000000, time % 1000000,
			g_random_int(),
			g_get_host_name());

		/* Get the flags from the original file name. */

		char *name = g_file_get_basename(file);
		char *pos = g_strrstr(name, ":");
		g_string_append(new_name, pos == NULL ? ":2,S" : pos);
		g_free(name);

		/* Move the file. */

		destination = g_file_get_child(directory, new_name->str);

		if (conf_verbose) {
			char *name = g_file_get_path(destination);
			g_print("   file: %s\n   dest: %s\n", filename, name);
			g_free(name);
		}

		if (conf_dry_run)
			goto end_file;

		if (!g_file_move(file, destination, G_FILE_COPY_NONE,
					NULL, NULL, NULL, error)) {
			result = FALSE;
			goto end_file;
		}

		/* Add the new path to the database. */

		name = g_file_get_path(destination);
		status = notmuch_database_index_file(db, name, NULL, NULL);
		g_free(name);

		if (status == NOTMUCH_STATUS_SUCCESS) {
			g_warning("Message should be a duplicate (%s)",
				notmuch_message_get_message_id(message));
		} else if (status != NOTMUCH_STATUS_DUPLICATE_MESSAGE_ID) {
			g_set_error(error, NOTMUCH_ERROR, status,
				"Notmuch error: %s",
				notmuch_status_to_string(status));
			result = FALSE;
			goto end_file;
		}

		/* Remove the old path from the database. */

		status = notmuch_database_remove_message(db, filename);

		if (status == NOTMUCH_STATUS_SUCCESS) {
			g_warning("A message was lost (%s)",
				notmuch_message_get_message_id(message));
		} else if (status != NOTMUCH_STATUS_DUPLICATE_MESSAGE_ID) {
			g_set_error(error, NOTMUCH_ERROR, status,
				"Notmuch error: %s",
				notmuch_status_to_string(status));
			result = FALSE;
			goto end_file;
		}

	end_file:
		g_clear_object(&destination);
		g_object_unref(file);
		g_object_unref(dir);
	}
	return result;
}

/*
 * Main entry point.
 */

gboolean
main_with_error (GError **error)
{
	notmuch_database_t *db;
	notmuch_query_t *query;
	notmuch_messages_t *messages;
	GFile *folder;

	if (!notmuch_check_status(
		notmuch_database_open_with_config(NULL,
			conf_dry_run ? NOTMUCH_DATABASE_MODE_READ_ONLY
				: NOTMUCH_DATABASE_MODE_READ_WRITE,
			NULL, NULL, &db, NULL), error))
		return FALSE;

	folder = g_file_new_for_path(notmuch_database_get_path(db));
	g_set_object(&folder, g_file_get_child(folder, conf_folder));
	conf_directory = g_file_get_child(folder, "cur");
	g_object_unref(folder);

	if (conf_verbose) {
		char *name = g_file_get_path(conf_directory);
		g_print("destination: %s\n", name);
		g_free(name);
	}

	query = notmuch_query_create(db, conf_query_string->str);
	gboolean result = notmuch_check_status(
		notmuch_query_search_messages(query, &messages), error);
	for (;
		result && notmuch_messages_valid(messages);
		result ? notmuch_messages_move_to_next(messages) : (void)0
	) {
		notmuch_message_t *message = notmuch_messages_get(messages);
		result = move_message(db, message, conf_directory, error);
		notmuch_message_destroy(message);
	}

	notmuch_query_destroy(query);
	notmuch_database_close(db);

	return result;
}
