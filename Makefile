## General configuration

BUILD = build
CFLAGS = -Wall -O3
PREFIX = /usr/local

PROGRESS = @echo
QUIET = @

.PHONY: all install

# Compiled tools

TOOLS := \
	notmuch-move \
	notmuch-propagate-tags \
	notmuch-query-addresses \
	notmuch-rm-dups \
	notmuch-sync-tags

all: $(addprefix $(BUILD)/,$(TOOLS))
install: $(addprefix $(PREFIX)/bin/,$(TOOLS))

PKG_CFLAGS = $(shell pkg-config --cflags ncurses glib-2.0 gio-2.0 gmime-3.0)
PKG_LDLIBS = -lnotmuch $(shell pkg-config --libs ncurses glib-2.0 gio-2.0)

$(BUILD)/%.dep: %.c
	$(PROGRESS) DEP $<
	@mkdir -p $(dir $@)
	$(QUIET) $(CC) $(CFLAGS) $(PKG_CFLAGS) -MT '$$(BUILD)/'$*.o -MM $< > $@

$(BUILD)/%.o: %.c
	$(PROGRESS) CC $@
	@mkdir -p $(dir $@)
	$(QUIET) $(CC) $(CFLAGS) $(PKG_CFLAGS) -c $< -o $@

$(addprefix $(BUILD)/,$(TOOLS)): %: %.o $(BUILD)/common.o
	$(PROGRESS) LD $@
	@mkdir -p $(dir $@)
	$(QUIET) $(CC) $(LDFLAGS) $^ -o $@ $(LDLIBS) $(PKG_LDLIBS)

$(PREFIX)/bin/%: $(BUILD)/%
	$(PROGRESS) INSTALL $@
	$(QUIET) install -d $(dir $@)
	$(QUIET) install $< $@

$(BUILD)/notmuch-query-addresses: $(BUILD)/sqlite.o
$(BUILD)/notmuch-query-addresses: LDLIBS += $(shell pkg-config --libs gmime-3.0 sqlite3)

$(BUILD)/notmuch-sync-tags: $(BUILD)/sqlite.o
$(BUILD)/notmuch-sync-tags: LDLIBS += $(shell pkg-config --libs sqlite3)

DEPS = $(patsubst %.c,$(BUILD)/%.dep,$(wildcard *.c))
-include $(DEPS)
.PHONY: dep
dep: $(DEPS)

# Scripts

$(PREFIX)/bin/notmuch-sync: notmuch-sync
	$(PROGRESS) INSTALL $@
	$(QUIET) install -d $(dir $@)
	$(QUIET) install $< $@

install: \
	$(PREFIX)/bin/notmuch-sync


# Documentation

MANPAGES := \
	notmuch-move.1 \
	notmuch-propagate-tags.1 \
	notmuch-query-addresses.1 \
	notmuch-rm-dups.1 \
	notmuch-sync.1 \
	notmuch-sync-tags.1

all: $(addprefix $(BUILD)/,$(MANPAGES))
install: $(addprefix $(PREFIX)/share/man/man1/,$(MANPAGES))

$(BUILD)/%.1: %.md
	$(PROGRESS) PANDOC $@
	@mkdir -p $(dir $@)
	$(QUIET) pandoc -f markdown-smart -t man -s $< -o $@

$(PREFIX)/share/man/man1/%.1: $(BUILD)/%.1
	$(PROGRESS) INSTALL $@
	$(QUIET) install -d $(dir $@)
	$(QUIET) install -m 644 $< $@
