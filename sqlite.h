#ifndef _SQLITE_H
#define _SQLITE_H

#include <glib.h>
#include <sqlite3.h>

/**
 * SQLITE3_ERROR:
 *
 * An error domain for SQLite error statuses. Errors in this domain are status
 * codes as used by the SQLite library.
 */

#define SQLITE3_ERROR sqlite_error_quark()

GQuark sqlite_error_quark (void);

/**
 * sqlite_check_status:
 * @status: a status value from SQLite
 * @error: a #GError
 *
 * Check that the status of a SQLite API call is a success and raise a
 * Glib-style error otherwise.
 *
 * Returns: %TRUE if @status is a success, otherwise %FALSE and @error is set
 */

gboolean sqlite_check_status (int status, GError **error);

/**
 * sqlite_call:
 * @database: an open SQLite database
 * @statement: a template SQL statement
 * @callback: a call-back function for results
 * @error: a #GError
 * @Varargs: values for the template
 *
 * This function is similar to sqlite3_exec() except that it accepts a
 * template and binds values to it, and it reads only a single statement.
 * Moreover, it uses #GError for error reporting.
 *
 * Values are specified as a list of pairs of arguments consisting in a type
 * (%SQLITE_INTEGER or %SQLITE_TEXT) followed by a value of the appropriate
 * type. The list is ended with a 0 as the argument type.
 *
 * The @callback function, if not %NULL, is called for each row of the result.
 * It receives as arguments:
 *  - the #sqlite3_stmt pointing to the current row,
 *  - a %va_list that points to the remaining arguments of sqlite_call(),
 *  - a #GError for error reporting.
 * If it fails, by returning %FALSE and setting the @error, then execution of
 * the statement is stopped.
 *
 * Returns: %TRUE on success, %FALSE on failure.
 */

gboolean
sqlite_call (
	sqlite3 *database,
	const char *statement,
	gboolean (*callback) (sqlite3_stmt *stmt, va_list args, GError **error),
	GError **error,
	...);

/**
 * get_values:
 * @stmt: a #sqlite3_stmt containing a row of data
 * @args: a #va_list with argument types and pointers
 * @error: a #GError
 *
 * Fill the pointers given in the @args argument list with values taken from
 * the current row in @stmt. The list of arguments in @args consists of pairs
 * of a #GType (among G_TYPE_INT and G_TYPE_ULONG) and a pointer to receive a
 * value of the appropriate type. The list is ended with a 0 as the argument
 * type.
 *
 * This function is a suitable callback for sqlite_call().
 *
 * Returns: always %TRUE.
 */

gboolean get_values (sqlite3_stmt *stmt, va_list args, GError **error);

/**
 * sqlite_dump:
 * @stmt: a #sqlite3_stmt
 *
 * Print the expanded SQL statement on standard output.
 */

void sqlite_dump (sqlite3_stmt *stmt);

extern gboolean conf_dump_sql;

/* Macros for simplifying SQL calls.
 *
 * The macros SQL and SQLv assume an open database in @sqlite_db (which can be
 * defined appropriately). All macros assume an error pointer in @error. In
 * case of failure, @error is set and control jumps to a label named @end.
 *
 * - SQL executes a simple statement
 * - SQLv executes a statement with parameters and callback, like @sqlite_call
 * - SQLITE calls an sqlite3 function and checks its return status
 */

#define SQL(statement) \
	do \
		if (!sqlite_call(sqlite_db, statement, NULL, error, 0)) \
			goto end; \
	while (0)

#define SQLv(statement,callback,...) \
	do \
		if (!sqlite_call(sqlite_db, statement, callback, error, __VA_ARGS__)) \
			goto end; \
	while (0)

#define SQLITE(fun,...) \
	do \
		if (!sqlite_check_status(sqlite3_##fun(__VA_ARGS__), error)) \
			goto end; \
	while (0)

#endif
