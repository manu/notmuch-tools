% notmuch-sync-tags(1) Notmuch-tools


# NAME

notmuch-sync-tags - synchronize tags in a notmuch database

# SYNOPSIS

**notmuch-sync-tags** [*option*...]

# DESCRIPTION

The **notmuch-sync-tags** command synchronises the message tags in a
message database managed by **notmuch**(1) with a tag repository
contained in an external database (an SQLite file).

The external repository essentially contains a tag set for each message
identifier (that is the information contained in a dump of the message
database as provided by **notmuch-dump**(1)) plus versioning information
to keep track of updates. At each call to **notmuch-sync-tags**, the
changes in message tags are examined and:

  - if the tags of a message changed in the repository, then the changes
  are incorporated in the message database,

  - if the tags of a message changed in the message database, then the
  changes are propagated to the repository.

If the tags of a message were changed in incompatible ways in the
message database and in the repository, by default the changes from the
repository have precedence, unless the **--push** switch is used.

The repository database can be safely used by several notmuch databases
holding common messages and it will effectively propagate changes
between the various databases by updating a common state. The UUID of
each message database is used to track the changes between successive
calls to **notmuch-sync-tags** from each of the databases. Hence the
file can be shared between mail stores by any file synchronization tool,
as long as concurrent accesses to the file are avoided. See below for an
example setup.

# OPTIONS

**-h**, **--help**
: Display a description of the command-line arguments and exit.

**-a**, **--all**
: Synchronise the tags of all messages, regardless of version
  information. This should normally have the same effect as the
  default behaviour which restricts to messages that have changed
  since the last synchronization, but it can be useful for consistency
  checks.

**-c**, **--clean**
: When a change is detected in a message in the repository but the
  message is not present in the message database, by default a warning
  message is issued with the identifier of the message. Using this
  option, the corresponding entry will be deleted from the repository
  instead.

**-p**, **--push**
: If a message has been changed both in the message database and in
  the repository and the changes differ, propagate the version of the
  database to the repository (without this option, the version from
  the repository is propagated to the database).

**-r**, **--repository**=FILE
: Set the name of the repository database file. The default name is
  `.notmuch/tags.db` in the mail store, as specified in the
  configuration option **database.path** in notmuch.

**-n**, **--dry-run**
: Simulate a synchronization but do not actually change the databases.
  When used in conjunction with **-v**, this will print out the
  changes that would occur without actually performing them.

**-v**, **--verbose**
: Print information on the synchronization process. This will report
  the changes pulled from the repository and pushed to it.

**-q**, **--quiet**
: Suppress progress reports. When this option is not used, simple
  progress reports are printed periodically to indicate how the
  process advances. This is useful for an initial run in a big
  database, or for a run that has to handle many changes.

**-s**, **--sql**
: Print the SQL queries that are sent to the SQLite database. This is
  mainly for debugging purposes, of course.

# ENVIRONMENT

**NOTMUCH_CONFIG**
: The location of the notmuch configuration file. By default, the path
  is `$HOME/.notmuch-config` as for **notmuch**(1). This path to the
  mail store is read from this file.

# EXAMPLE

This tool is designed to be used together with mailbox synchronization and
indexing. The typical usage pattern consists in fetching the tag database from
a remote repository when synchronizing mailboxes with a tool like
**mbsync**(1) or **offlineimap**(1), with a command like

    rsync -tu my-server:tags.db $(notmuch config get database.path)/.notmuch/tags.db

Then new messages are indexed using `notmuch new` and the **post-new** hook
starts by integrating any changes that have not been applied yet in the local
database, simply by calling

    notmuch-sync-tags

before doing any initial tagging operations. Note that if a message is known
in `tags.db` and has already been tagged there, this will effectively remove
the `new` tag used for initial tagging. Then, after the rest of initial
tagging is done, synchronize back the tag repository:

    rsync -tu $(notmuch config get database.path)/.notmuch/tags.db my-server:tags.db

Note that **notmuch-sync-tags** is designed to make this work properly, even
if the same scripts are used in several notmuch databases that synchronize to
the same repository. The calls to **rsync**(1) can be included in the
**pre-sync** and **post-sync** hooks provided by **notmuch-sync**(1) or in
similar features provided by the mailbox synchronization tool.

The only limit is that there may be a race condition with the **rsync**
operations: some information will be lost if the remote `tags.db` is changed
between the two calls to **rsync** in the above process.

# AUTHOR

Emmanuel Beffara <manu@beffara.org>

# SEE ALSO

**notmuch**(1)**, ****notmuch-config**(1)
