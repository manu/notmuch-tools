#include <glib.h>
#include <gio/gio.h>
#include <notmuch.h>

#include "common.h"

/* Configuration variables. */

char **conf_ignore = NULL;
GString * conf_query_string = NULL;

GPtrArray *ignored;

/* Command line. */

static const char *parameter_string =
"[TERMS...] - propagate tags in threads in a notmuch database";

static const char *summary =
"Positional arguments:\n"
"  TERMS            Search terms for messages to propagate to";

static const GOptionEntry entries[] =
{
	{ "ignore", 'i', 0, G_OPTION_ARG_STRING_ARRAY, &conf_ignore,
		"Do not propagate this tag", "TAG" },
	{ "dry-run", 'n', 0, G_OPTION_ARG_NONE, &conf_dry_run,
		"Do not perform operations", NULL },
	{ "verbose", 'v', 0, G_OPTION_ARG_NONE, &conf_verbose,
		"Show operations before doing them", NULL },
	{ NULL }
};

gboolean
parse_commandline (int *argc, char ***argv, GError **error)
{
	GOptionContext *context;
	gboolean status = FALSE;
	int i;

	context = g_option_context_new(parameter_string);
	g_option_context_set_summary(context, summary);
	g_option_context_add_main_entries(context, entries, NULL);

	if (!g_option_context_parse(context, argc, argv, error)) goto end;

	conf_query_string = g_string_new("");
	if (*argc < 2)
		g_string_assign(conf_query_string, "tag:new");
	else for (i = 1; i < *argc; i++) {
		if (conf_query_string->len > 0)
			g_string_append_c(conf_query_string, ' ');
		g_string_append(conf_query_string, (*argv)[i]);
	}

	status = TRUE;
end:
	g_option_context_free(context);
	return status;
}

/**
 * propagate_in_message:
 * @message: a Notmuch message to propagate to
 * @tags: an %NULL-terminated array of tags to propagate
 * @depth: the depth of the message in the thread (for verbose messages)
 * @error: a #GError
 *
 * Propagate tags to a message and its replies. If the message matches the
 * quey it was obtained from, then tags are actually added to the message and
 * propagated to the replies, along with any tags the message already
 * contains. If it does not match, then @tags is ignored and tags from this
 * message are propagated to the replies instead.
 *
 * Returns: %TRUE on success.
 */

static const char * const empty_tag_list[1] = { NULL };

static gboolean
propagate_in_message (
	notmuch_message_t *message,
	const char * const *tags,
	int depth,
	GError **error)
{
	notmuch_bool_t matches;
	const char * const *ptags;
	GPtrArray *propagated = g_ptr_array_new_full(8, g_free);
	notmuch_messages_t *messages;
	gboolean result = FALSE;

	NOTMUCH(message_get_flag_st, message,
		NOTMUCH_MESSAGE_FLAG_MATCH, &matches);

	if (conf_verbose) {
		int i;
		for (i = 0; i < depth; i++) g_print("  ");
		g_print("%s %s (",
			matches ? "+" : "-",
			notmuch_message_get_message_id(message));
	}

	/* Clear the @tags argument if the message does not match. */

	if (!matches)
		tags = empty_tag_list;

	/* Enumerate the tags in @message that are not already in
	 * @tags. */

	notmuch_tags_t *mtags = notmuch_message_get_tags(message);
	for (; notmuch_tags_valid(mtags); notmuch_tags_move_to_next(mtags)) {
		const char *tag = notmuch_tags_get(mtags);
		if (g_ptr_array_find_with_equal_func(ignored, tag, g_str_equal, NULL)) continue;
		if (conf_verbose) g_print(" %s", tag);
		if (!g_strv_contains(tags, tag)) {
			if (matches && conf_verbose) g_print("*");
			g_ptr_array_add(propagated, g_strdup(tag));
		}
	}
	notmuch_tags_destroy(mtags);
	if (conf_verbose) g_print(" )");

	/* Add the @tags to the message and complete the propagated tag list. */

	for (ptags = tags; *ptags != NULL; ptags++) {
		if (g_ptr_array_find_with_equal_func(propagated, *ptags, g_str_equal, NULL)) continue;
		g_ptr_array_add(propagated, g_strdup(*ptags));
		if (!matches) continue;
		if (conf_verbose) g_print(" +%s", *ptags);
		if (conf_dry_run) continue;
		NOTMUCH(message_add_tag, message, *ptags);
	}

	g_ptr_array_add(propagated, NULL);

	if (conf_verbose) {
		g_print(" >>");
		for (ptags = (const char* const*)propagated->pdata; *ptags != NULL; ptags++)
			g_print(" %s", *ptags);
		g_print("\n");
	}

	/* Propagate to the replies. */

	messages = notmuch_message_get_replies(message);
	while (notmuch_messages_valid(messages)) {
		notmuch_message_t *reply = notmuch_messages_get(messages);
		if (!propagate_in_message(reply,
			(const char* const*)propagated->pdata, depth + 1, error))
			goto end;
		notmuch_messages_move_to_next(messages);
	}
	result = TRUE;
end:
	g_ptr_array_free(propagated, TRUE);
	return result;
}

/**
 * propagate_in_thread:
 * @db: the Notmuch database holding the thread (must be writeable)
 * @message: a Notmuch thread to propagate in
 * @error: a #GError
 *
 * Propagate tags from messages inside a thread. Messages that match the query
 * used to get the thread will be tagged by any tag their parent has. Messages
 * that do not match the query are unmodified.
 *
 * Returns: %TRUE on success.
 */

static gboolean
propagate_in_thread (notmuch_thread_t *thread, GError **error)
{
	notmuch_messages_t *messages;
	gboolean result = FALSE;

	if (conf_verbose) g_print("thread %s\n", notmuch_thread_get_thread_id(thread));

	messages = notmuch_thread_get_toplevel_messages(thread);
	while (notmuch_messages_valid(messages)) {
		notmuch_message_t *message = notmuch_messages_get(messages);
		if (!propagate_in_message(message, empty_tag_list, 0, error))
			goto end;
		notmuch_messages_move_to_next(messages);
	}
	result = TRUE;
end:
	notmuch_messages_destroy(messages);
	return result;
}


/*
 * Main entry point.
 */

gboolean
main_with_error (GError **error)
{
	gboolean result = FALSE;
	notmuch_database_t *db;
	notmuch_query_t *query;
	notmuch_threads_t *threads;

	ignored = g_ptr_array_new();
	g_ptr_array_add(ignored, "attachment");
	g_ptr_array_add(ignored, "deleted");
	g_ptr_array_add(ignored, "encrypted");
	g_ptr_array_add(ignored, "flagged");
	g_ptr_array_add(ignored, "replied");
	g_ptr_array_add(ignored, "sent");
	g_ptr_array_add(ignored, "signed");
	g_ptr_array_add(ignored, "unread");
	if (conf_ignore) {
		char **p;
		for (p = conf_ignore; *p != NULL; p++)
			g_ptr_array_add(ignored, *p);
	}

	NOTMUCH(database_open_with_config, NULL,
		conf_dry_run ? NOTMUCH_DATABASE_MODE_READ_ONLY
			: NOTMUCH_DATABASE_MODE_READ_WRITE,
		NULL, NULL, &db, NULL);

	query = notmuch_query_create(db, conf_query_string->str);
	NOTMUCH(query_search_threads, query, &threads);
	while (notmuch_threads_valid(threads)) {
		notmuch_thread_t *thread = notmuch_threads_get(threads);
		gboolean status = propagate_in_thread(thread, error);
		notmuch_thread_destroy(thread);
		if (!status) return FALSE;
		notmuch_threads_move_to_next(threads);
	}

	notmuch_query_destroy(query);
	notmuch_database_close(db);
	result = TRUE;
end:
	return result;
}
