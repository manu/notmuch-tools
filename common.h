#ifndef _COMMON_H
#define _COMMON_H

#include <glib.h>
#include <gio/gio.h>
#include <notmuch.h>

/* g_set_object exists only since Glib version 2.44 */

#if ! GLIB_CHECK_VERSION(2, 44, 0)

static inline gboolean
g_set_object (GObject **object_ptr, GObject  *new_object)
{
	if (*object_ptr == new_object) return FALSE;
	if (new_object != NULL) g_object_ref (new_object);
	if (*object_ptr != NULL) g_object_unref (*object_ptr);
	*object_ptr = new_object;
	return TRUE;
}

#define g_set_object(object_ptr, new_object) \
	(/* Check types match. */ \
	 0 ? *(object_ptr) = (new_object), FALSE : \
	 (g_set_object) ((GObject **) (object_ptr), (GObject *) (new_object)) \
	 )

#endif

/**
 * NT_ERROR:
 *
 * An error domain for errors specific to notmuch-tools programs. Errors in
 * this domain will be from the #NTErrorEnum enumeration.
 */

#define NT_ERROR nt_error_quark()

GQuark nt_error_quark (void);

typedef enum {
	NT_ERROR_MISSING_CONFIG,
	NT_ERROR_BAD_CONFIG,
	NT_ERROR_QUERY_EMPTY,
	NT_ERROR_MESSAGE_NOT_FOUND,
	NT_ERROR_CONFLICT,
	NT_ERROR_MESSAGE_PARSE_ERROR,
	NT_ERROR_MESSAGE_WRITE_ERROR,
	NT_ERROR_BAD_PATH,
	NT_ERROR_MISSING_CACHE,
} NTErrorEnum;


/**
 * NOTMUCH_ERROR:
 *
 * An error domain for Notmuch error statuses. Errors in this domain are
 * %notmuch_status_t values.
 */

#define NOTMUCH_ERROR notmuch_error_quark()

GQuark notmuch_error_quark (void);

/**
 * notmuch_check_status:
 * @status: a status value from notmuch
 * @error: a #GError
 *
 * Check that the status of a notmuch API call is a success and raise a
 * Glib-style error otherwise.
 *
 * Returns: %TRUE if @status is a success, otherwise %FALSE and @error is set
 */

gboolean notmuch_check_status (notmuch_status_t status, GError **error);

/**
 * NOTMUCH:
 * @fun: a function name, without the notmuch prefix
 *
 * Call a function from the notmuch library and check its return status. If
 * the status is not a success, set the @error variable in the current scope
 * and jump to the @end label.
 */

#define NOTMUCH(fun,...) \
	do \
		if (!notmuch_check_status(notmuch_##fun(__VA_ARGS__), error)) \
			goto end; \
	while (0)

/* General configuration variables.
 */

extern gboolean conf_dry_run;
extern gboolean conf_quiet;
extern gboolean conf_verbose;

/**
 * progress_start:
 * @interval: a time interval between successive messages
 *
 * Start a thread that prints progress reports at regular intervals.
 *
 * Progress reports are defined by the following global variables.
 * - @progress_phase is a string that indicates what is happening
 * - @progress_total is an unsigned integer that indicates the number of items
 *   to be handled in the current phase (if it is zero, then no numering
 *   progress is displayed)
 * - @progress_count is the number of items already handled.
 * The main process may freely update these variables when running, the
 * prgress reports will use the current values when printing its messages.
 */

void progress_start (gint64 interval);

/**
 * progress_stop:
 *
 * Stop printing progress reports.
 */

void progress_stop ();

extern const char * progress_phase;
extern unsigned int progress_count;
extern unsigned int progress_total;

/**
 * parse_commandline:
 * @argc: a pointer to main's @argc
 * @argv: a pointer to main's @argv
 * @error: a #GError
 *
 * Parse the command line and define the configuration accordingly.
 *
 * Returns: %TRUE on success
 */

gboolean parse_commandline (int *argc, char ***argv, GError **error);

/**
 * main_with_error:
 * @error: a #GError
 *
 * The main entry point, using the Glib error handling system.
 * It is called after the command line has been parsed.
 *
 * Returns: %TRUE on successful run, %FALSE on failure.
 */

gboolean main_with_error (GError **error);

#endif
