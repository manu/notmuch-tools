% notmuch-sync(1) Notmuch-tools


# NAME

notmuch-sync - synchronize mailboxes and index them with Notmuch

# SYNOPSIS

**notmuch-sync** [*option*...] [ [--] *sync-options*...]

# DESCRIPTION

The **notmuch-sync** command is a simple script that calls an external
command to synchronize mailboxes and uses **notmuch**(1) to index them.
Extra hooks are provided to run custom commands before and after
synchronization.

# CONFIGURATION

The following settings are read from the configuration file of notmuch,
following the conventions described in **notmuch-config**(1). The parameters
are in a specific section introduced by `[sync]` in the configuration file.

**sync.command**
: A shell command to fetch new mail. The command is supposed to *not*
  call **notmuch new****, **because **notmuch-sync** will take care of
  that. The extra command-line arguments *sync-options* will be passed
  to this command as the value of the `$@` shell parameter.
  The variables `$QUIET` and `$VERBOSE` are set to `true` or `false` according
  to the presence of the command-line arguments **-q** and **-v**.

**sync.lock_file**
: The name of a file acting as a lock to prevent two synchronizations from
  happening concurrently. If this is set and the file exists, then
  synchronization is not done, otherwise it is created for the duration of
  synchronization. If this option is unset, no locking happens.

# OPTIONS

**-a**, **--args**=*ARGS*
: Pass the arguments *ARGS* to the call to `notmuch new` when
  indexing.

**-h**, **--help**
: Display a description of the command-line arguments and exit.

**-n**, **--dry-run**
: Simulate a run but do not actually call commands that may change the
  mailboxes or database.

**-q**, **--quiet**
: Suppress all informational messages, display only error messages.

**-v**, **--verbose**
: Display operations before doing them.

**-w**, **--watch**
: Synchronize mailboxes continuously instead of synchronizing just
  once.

**--no-hooks**
: Do not run the **pre-sync** and **post-sync** hooks during synchronization.

Any option not in this list marks the end of the *options* list, and
**--** explicitly marks the end of this list. Remaining options are
passed to the mailbox synchronization command.

# HOOKS

The normal indexing operation **notmuch-new**(1) supports the hooks
**pre-new** and **post-new** and these tags are respected by
**notmuch-sync**. The following hooks are added:

**pre-sync**
: This hook is invoked before mailboxes are synchronized. If this hook
  exits with a non-zero status, **notmuch-sync** will abort the
  synchronization procedure. This hook is typically used to actually
  remove the messages that are marked as deleted.

**post-sync**
: This hook is invoked after mailboxes are synchronized and indexed.

# ENVIRONMENT

**NOTMUCH_CONFIG**
: The location of the notmuch configuration file. By default, the path
  is `$HOME/.notmuch-config` as for **notmuch**(1). This path to the
  mail store is read from this file.

# EXAMPLE

This script was written in order to wrap the functionality of **mbsync**(1),
which does not provide a hook mechanism out of the box. This can be configured
by appending this to `~/.notmuch-config`:

    [sync]
    command=mbsync $($QUIET && echo -q) "${@:--a}"

Note that this adds **-q** if **notmuch-sync** is called with that option and
adds **-a** if the *sync-options* are empty, so that synchronization fetches
messages from all channels by default.

A **pre-sync** hook could be used to perform some operations on messages, for
instance using **notmuch-move**(1) to move deleted messages to a Trash folder
that is not synchronized by **mbsync**:

    #/bin/sh
    notmuch tag -flagged -inbox -unread tag:deleted and not folder:Trash
    notmuch-move Trash tag:deleted and not folder:Trash

See the documentation of **notmuch-sync-tags**(1) for way to combine mailbox
synchronization with tag synchronization with a remote repository.

# AUTHOR

Emmanuel Beffara <manu@beffara.org>

# SEE ALSO

**notmuch**(1), **notmuch-config**(1), **notmuch-hooks**(5)
