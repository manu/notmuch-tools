#include <glib.h>
#include <gio/gio.h>
#include <notmuch.h>
#include <sqlite3.h>

#include "common.h"
#include "sqlite.h"

/* Configuration variables. */

gboolean conf_sync_all = FALSE;
gboolean conf_clean = FALSE;
gboolean conf_push = FALSE;
char * conf_repo = NULL;

/* Global state. */

sqlite3 *repository = NULL;
#define sqlite_db repository
notmuch_database_t *database = NULL;
unsigned int total_missing = 0;

GString *buffer = NULL;

/* Command line. */

static const char *parameter_string =
"- synchronise tags in a notmuch database";

static const GOptionEntry option_entries[] =
{
	{ "all", 'a', 0, G_OPTION_ARG_NONE, &conf_sync_all,
		"Synchronise all messages, not just updated ones", NULL },
	{ "clean", 'c', 0, G_OPTION_ARG_NONE, &conf_clean,
		"Drop non-existent messages from the database", NULL },
	{ "push", 'p', 0, G_OPTION_ARG_NONE, &conf_push,
		"In case of conflict, propagate local changes", NULL },
	{ "repository", 'r', 0, G_OPTION_ARG_FILENAME, &conf_repo,
		"Filename for the repository database", "FILE" },
	{ "dry-run", 'n', 0, G_OPTION_ARG_NONE, &conf_dry_run,
		"Do not perform operations", NULL },
	{ "verbose", 'v', 0, G_OPTION_ARG_NONE, &conf_verbose,
		"Print information on the synchronisation process", NULL },
	{ "quiet", 'q', 0, G_OPTION_ARG_NONE, &conf_quiet,
		"Suppress progress messages", NULL },
	{ "dump-sql", 's', 0, G_OPTION_ARG_NONE, &conf_dump_sql,
		"Print the SQL queries before doing them", NULL },
	{ NULL }
};

gboolean
parse_commandline (int *argc, char ***argv, GError **error)
{
	gboolean status = FALSE;

	/* Parse the command line. */

	GOptionContext *context = g_option_context_new(parameter_string);
	g_option_context_set_summary(context, NULL);
	g_option_context_add_main_entries(context, option_entries, NULL);

	if (!g_option_context_parse(context, argc, argv, error)) goto end;
	if (*argc > 1) {
		g_set_error_literal(error,
			G_OPTION_ERROR, G_OPTION_ERROR_FAILED,
			"Too many arguments");
		goto end;
	}

	status = TRUE;
end:
	g_option_context_free(context);
	return status;
}


/**
 * extract_tags:
 * @revision: limit to messages with at least this revision number
 * @error: a #GError
 *
 * Extract tags from the Notmuch database into a temporary table "local".
 *
 * Returns: %TRUE on success
 */

static gboolean
extract_tags (unsigned long revision, GError **error)
{
	gboolean result = FALSE;
	sqlite3_stmt *stmt = NULL;
	notmuch_messages_t *messages;
	notmuch_query_t *query;
	int status;

	progress_phase = "Extracting tags";

	SQL("CREATE TEMPORARY TABLE local ("
		"message_id TEXT NOT NULL PRIMARY KEY ON CONFLICT REPLACE,"
		"tags TEXT NOT NULL)");

	SQLITE(prepare_v2, repository,
		"INSERT INTO local VALUES (?, ?)", -1, &stmt, NULL);

	char *query_text = g_strdup_printf("lastmod:%lu..", revision);
	query = notmuch_query_create(database, query_text);
	g_free(query_text);

	if (!conf_quiet || conf_verbose)
		NOTMUCH(query_count_messages, query, &progress_total);
	if (conf_verbose)
		g_print("Local changes: %u\n", progress_total);

	NOTMUCH(query_search_messages, query, &messages);
	for (;
		notmuch_messages_valid(messages);
		notmuch_messages_move_to_next(messages)
	) {
		notmuch_message_t *message = notmuch_messages_get(messages);
		notmuch_tags_t *tags;

		/* Set the message ID in the query. */

		SQLITE(bind_text, stmt, 1,
			notmuch_message_get_message_id(message),
			-1, SQLITE_STATIC);

		/* Build the tag list and set it in the query. */

		g_string_truncate(buffer, 0);
		for (tags = notmuch_message_get_tags (message);
				notmuch_tags_valid (tags);
				notmuch_tags_move_to_next (tags)) {
			if (buffer->len > 0) g_string_append_c(buffer, ' ');
			g_string_append(buffer, notmuch_tags_get(tags));
		}

		SQLITE(bind_text, stmt, 2, buffer->str, -1, SQLITE_STATIC);

		/* Run the query. */

		if (conf_dump_sql) sqlite_dump(stmt);

		switch (status = sqlite3_step(stmt)) {
		case SQLITE_DONE: break;
		default: if (!sqlite_check_status(status, error)) goto end;
		}
		sqlite3_reset(stmt);

		progress_count ++;
	}

	result = TRUE;
end:
	if (stmt != NULL) sqlite3_finalize(stmt);
	return result;
}


/**
 * update_message:
 * @stmt: a #sqlite3_stmt containing a row of data
 * @args: a #va_list with unsigned long pointers
 * @error: a #GError
 *
 * Read the current row in @stmt as a message ID and a space-separated list of
 * tags and update the associated message in the Notmuch database with the
 * tags from this list.
 *
 * If no message with the expected ID is found, a warning message is printed
 * but no error is reported.
 *
 * Returns: %TRUE on success, %FALSE if some notmuch operation failed.
 */

static gboolean
update_message (sqlite3_stmt *stmt, va_list args, GError **error)
{
	notmuch_message_t *message = NULL;
	gboolean result = FALSE;
	const char *id, *tags;
	char *pos, *next, *end;

	id = (const char*) sqlite3_column_text(stmt, 0);
	tags = (const char*) sqlite3_column_text(stmt, 1);

	NOTMUCH(database_find_message, database, id, &message);

	if (message == NULL) {
		if (!conf_clean) {
			if (conf_verbose) g_printerr("Missing message: %s\n", id);
			total_missing++;
			result = TRUE;
			goto end;
		}
		if (conf_verbose)
			g_print("Drop %s\n", id);
		if (!conf_dry_run)
			SQLv("DELETE FROM tags WHERE message_id = ?",
				NULL, G_TYPE_STRING, id, 0);
		result = TRUE;
		goto end;
	}

	if (conf_verbose)
		g_print("Pull (%s) for %s\n", tags, id);

	if (conf_dry_run) {
		result = TRUE;
		goto end;
	}

	NOTMUCH(message_freeze, message);
	NOTMUCH(message_remove_all_tags, message);

	g_string_assign(buffer, tags);
	pos = buffer->str;
	end = pos + buffer->len;
	while (pos < end) {
		next = pos;
		while (next < end && *next != ' ') next++;
		if (*next != 0) *next = 0;
		NOTMUCH(message_add_tag, message, pos);
		pos = next + 1;
	}

	NOTMUCH(message_thaw, message);

	result = TRUE;
end:
	if (message != NULL) notmuch_message_destroy(message);
	progress_count ++;
	return result;
}

static gboolean
show_push (sqlite3_stmt *stmt, va_list args, GError **error)
{
	g_print("Push (%s) for %s\n",
		sqlite3_column_text(stmt, 1),
		sqlite3_column_text(stmt, 0));
	return TRUE;
}

/*
 * Main entry point.
 */

gboolean
main_with_error (GError **error)
{
	gboolean result = FALSE;
	sqlite3_stmt *stmt = NULL;
	const char *uuid;
	unsigned long revision, version, last_revision, last_version, count;

	buffer = g_string_new("");

	/* Start progress reports. */

	if (!conf_quiet) {
		progress_phase = "Initialisation";
		progress_start(G_TIME_SPAN_SECOND/2);
	}

	/* Open the notmuch database. */

	NOTMUCH(database_open_with_config, NULL,
		conf_dry_run ? NOTMUCH_DATABASE_MODE_READ_ONLY
			: NOTMUCH_DATABASE_MODE_READ_WRITE,
		NULL, NULL, &database, NULL);

	/* Set the default path to the repository database if needed. */

	if (conf_repo == NULL)
		conf_repo = g_strdup_printf("%s/.notmuch/tags.db",
			notmuch_database_get_path(database));

	/* Get the UUID and revision of the local database. */

	revision = notmuch_database_get_revision(database, &uuid);

	if (conf_verbose)
		g_print("UUID: %s\nCurrent revision: %lu\n", uuid, revision);

	/* Open the SQLite database. */

	SQLITE(open, conf_repo, &repository);

	/* Create the tables if needed. */

	SQL("CREATE TABLE IF NOT EXISTS tags ("
		"message_id TEXT NOT NULL PRIMARY KEY ON CONFLICT REPLACE,"
		"tags TEXT NOT NULL,"
		"version INTEGER NOT NULL)");

	SQL("CREATE TABLE IF NOT EXISTS states ("
		"uuid TEXT NOT NULL PRIMARY KEY ON CONFLICT REPLACE,"
		"revision INTEGER NOT NULL,"
		"version INTEGER NOT NULL)");

	SQL("BEGIN");

	/* Get the current version of the database. */

	version = 0;
	SQLv("SELECT MAX(version) FROM tags", get_values, 0,
		G_TYPE_ULONG, &version, 0);
	
	if (conf_verbose)
		g_print("Current version: %lu\n", version);

	/* Get the revision and version for the last synchronisation. */

	last_revision = last_version = 0;
	SQLv("SELECT revision, version FROM states WHERE uuid=?",
		get_values,
		G_TYPE_STRING, uuid, 0,
		G_TYPE_ULONG, &last_revision, G_TYPE_ULONG, &last_version, 0);

	if (conf_verbose)
		g_print("Last revision: %lu\nLast version: %lu\n",
			last_revision, last_version);

	/* Create a temporary table with changes in the repository. */

	progress_phase = "Extracting remote changes";

	if (conf_sync_all)
		SQL("CREATE TEMPORARY TABLE remote AS "
			"SELECT message_id, tags FROM tags");
	else
		SQLv("CREATE TEMPORARY TABLE remote AS "
			"SELECT message_id, tags FROM tags WHERE version>?",
			NULL, G_TYPE_ULONG, last_version, 0);

	if (conf_verbose) {
		SQLv("SELECT COUNT(message_id) FROM remote",
			get_values, 0, G_TYPE_ULONG, &count, 0);
		g_print("Remote changes: %lu\n", count);
	}

	/* Create a temporary table with changes in the Notmuch database. */

	if (!extract_tags(conf_sync_all ? 0 : last_revision + 1, error))
		goto end;

	/* Resolve conflicts. */

	progress_total = 0;
	progress_phase = "Resolving conflicts";

	SQL("CREATE TEMPORARY TABLE inter AS "
		"SELECT local.message_id FROM local INNER JOIN remote "
		"ON local.message_id = remote.message_id "
		"AND local.tags = remote.tags");

	if (conf_verbose) {
		SQLv("SELECT COUNT(message_id) FROM inter",
			get_values, 0, G_TYPE_ULONG, &count, 0);
		g_print("Compatible changes: %lu\n", count);
	}


	if (conf_push) {
		SQL("DELETE FROM remote "
			"WHERE message_id IN (SELECT message_id FROM local)");
		SQL("DELETE FROM local "
			"WHERE message_id IN (SELECT message_id FROM inter)");
	} else {
		SQL("DELETE FROM local "
			"WHERE message_id IN (SELECT message_id FROM remote)");
		SQL("DELETE FROM remote "
			"WHERE message_id IN (SELECT message_id FROM inter)");
	}

	/* Propagate local changes. */

	progress_phase = "Propagating local changes";

	SQLv("SELECT COUNT(message_id) FROM local",
		get_values, 0, G_TYPE_ULONG, &count, 0);

	if (conf_verbose)
		g_print("Pushed changes: %lu\n", count);
	if (count != 0)
		version ++;

	if (conf_verbose)
		SQLv("SELECT * FROM local", show_push, 0);

	if (!conf_dry_run)
		SQLv("INSERT INTO tags SELECT message_id, tags, ? FROM local",
			NULL, G_TYPE_ULONG, version, 0);

	/* Incorporate remote changes into the Notmuch database. */

	progress_phase = "Incorporating remote changes";

	NOTMUCH(database_begin_atomic, database);

	if (!conf_quiet)
		SQLv("SELECT COUNT(message_id) FROM remote",
			get_values, 0, G_TYPE_ULONG, &progress_total, 0);
	if (conf_verbose)
		g_print("Pulled changes: %u\n", progress_total);

	SQLv("SELECT message_id, tags FROM remote", update_message, 0);

	NOTMUCH(database_end_atomic, database);

	if (!conf_quiet) {
		if (total_missing == 1)
			g_print("1 message was missing.\n");
		else if (total_missing > 1)
			g_print("%u messages were missing.\n", total_missing);
	}

	/* Update the versions and revisions. */

	revision = notmuch_database_get_revision(database, &uuid);
	if (conf_verbose)
		g_print("New revision: %lu\nNew version: %lu\n",
			revision, version);

	if (!conf_dry_run)
		SQLv("INSERT INTO states VALUES (?, ?, ?)",
			NULL,
			G_TYPE_STRING, uuid,
			G_TYPE_ULONG, revision,
			G_TYPE_ULONG, version,
			0);

	SQL("COMMIT");

	result = TRUE;
end:
	if (!conf_quiet) progress_stop();
	if (buffer != NULL) g_string_free(buffer, TRUE);
	if (stmt != NULL) sqlite3_finalize(stmt);
	if (database != NULL) notmuch_database_close(database);
	if (repository != NULL) sqlite3_close(repository);
	return result;
}
