% notmuch-propagate-tags(1) Notmuch-tools


# NAME

notmuch-propagate-tags - propagate tags in threads in a notmuch database

# SYNOPSIS

**notmuch-propagate-tags** [*option*...] [*search-term*...]

# DESCRIPTION

The **notmuch-propagate-tags** command adds tags to messages in
a **notmuch**(1) database using tags from parent messages. The search terms
indicates which messages will be tagged. For each such message that is a reply
to some parent message (which may not itself match the search terms), the tags
of the parent are applied to the reply.

Some tags are not taken into account because they inherently apply to single
messages and propagating them does not make sense: **attachment**,
**deleted**, **encrypted**, **flagged**, **replied**, **sent**, **signed** and **unread**
are always ignored. Additional tags can be ignored using the **-i** flag.

# OPTIONS

**-h**, **--help**
: Display a description of the command-line arguments and exit.

**-i**, **--ignore**=*TAG*
: Add *TAG* to the list of tags that are not propagated.

**-n**, **--dry-run**
: Simulate tag propagation but do not actually change the local database.

**-v**, **--verbose**
: Display operations before doing them.

# ENVIRONMENT

**NOTMUCH_CONFIG**
: The location of the notmuch configuration file. By default, the path
  is `$HOME/.notmuch-config` as for **notmuch**(1).

# AUTHOR

Emmanuel Beffara <manu@beffara.org>

# SEE ALSO

**notmuch**(1)
