% notmuch-move(1) Notmuch-tools


# NAME

notmuch-move - move messages in a notmuch database

# SYNOPSIS

**notmuch-move** [*option*...] *folder* *search-term*...

# DESCRIPTION

The **notmuch-move** command moves message files for messages indexed in
a **notmuch**(1) database. The *folder* name is interpreted relative to
the root of the mail store. The search terms indicates which messages to
move. The database is updated with the new locations of the files. If a
message has several files, then all files are moved to the same
destination folder.

# OPTIONS

**-h**, **--help**
: Display a description of the command-line arguments and exit.

**-n**, **--dry-run**
: Simulate a move but do not actually change the local database.

**-v**, **--verbose**
: Display operations before doing them.

# ENVIRONMENT

**NOTMUCH_CONFIG**
: The location of the notmuch configuration file. By default, the path
  is `$HOME/.notmuch-config` as for **notmuch**(1). This path to the
  mail store is read from this file.

# AUTHOR

Emmanuel Beffara <manu@beffara.org>

# SEE ALSO

**notmuch**(1)
