This is a set of small command-line tools for the
[Notmuch](http://notmuchmail.org/) mail indexer.

Installing
----------

In order to compile these tools, you need the development files for
`libnotmuch` and for the packages `glib-2.0`, `gmime-3.0` and `sqlite3`. You
will also need [Pandoc](https://pandoc.org/) in order to make the man pages.
Just say

    make install

and it will compile the tools and install them in `/usr/local`. An alternative
destination can be specified using the `PREFIX` variable:

    make install PREFIX=/usr

Tools
-----

 - `notmuch-move` moves message files between mail directories according to
   search queries  
 - `notmuch-propagate-tags` tags messages using tags from the messages they
   reply to
 - `notmuch-query-addresses` searches for addresses matching a given pattern
   in the message database
 - `notmuch-rm-dups` deletes duplicate message files, keeping one file per
   message
 - `notmuch-sync` is a small script to fetch messages (with a custom command)
   and index them with support for specific hooks
 - `notmuch-sync-tags` synchronises the tags with a tag repository contained
   in an external database

Each of these tools supports a `--help` option that provides usage
information. Documentation for each tool is provided as a man page in the
`docs` folder, including usage examples.
