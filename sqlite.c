#include <glib-object.h>

#include "sqlite.h"

gboolean conf_dump_sql;

/* Sqlite errors, the Glib way */

GQuark sqlite_error_quark (void)
{
	return g_quark_from_static_string ("sqlite-error-quark");
}

gboolean
sqlite_check_status (int status, GError **error)
{
	if (status == SQLITE_OK)
		return TRUE;
	g_set_error(error, SQLITE3_ERROR, status,
			"SQLite error: %s", sqlite3_errstr(status));
	return FALSE;
}

gboolean
sqlite_call (
	sqlite3 *database,
	const char *statement,
	gboolean (*callback) (sqlite3_stmt *stmt, va_list args, GError **error),
	GError **error,
	...)
{
	sqlite3_stmt *stmt = NULL;
	va_list args;
	int value_index, status;
	gboolean args_set = FALSE, result = FALSE;

	/* Prepare the statement. */

	if (!sqlite_check_status(sqlite3_prepare_v2(database,
		statement, -1, &stmt, NULL), error)) goto end;

	/* Bind the values. */

	va_start(args, error); args_set = TRUE;
	value_index = 0;
	for (;;) switch (va_arg(args, int)) {
	case 0:
		goto end_args;

	case G_TYPE_INT:
		status = sqlite3_bind_int(stmt, ++value_index, va_arg(args, int));
		if (!sqlite_check_status(status, error)) goto end;
		break;

	case G_TYPE_ULONG:
		status = sqlite3_bind_int(stmt, ++value_index, va_arg(args, unsigned long));
		if (!sqlite_check_status(status, error)) goto end;
		break;

	case G_TYPE_STRING:
		status = sqlite3_bind_text(stmt, ++value_index,
			va_arg(args, const char *), -1, SQLITE_STATIC);
		if (!sqlite_check_status(status, error)) goto end;
		break;

	default:
		/* Unsupported type. */
		g_assert_not_reached();
	}
end_args:

	if (conf_dump_sql) sqlite_dump(stmt);

	/* Step through the results. */

	for (;;) switch (status = sqlite3_step(stmt)) {
	case SQLITE_DONE:
		result = TRUE;
		goto end;

	case SQLITE_ROW:
		if (callback != NULL) {
			va_list args_copy;
			gboolean callback_result;
			va_copy(args_copy, args);
			callback_result = callback(stmt, args_copy, error);
			va_end(args_copy);
			if (!callback_result) goto end;
		}
		break;

	default:
		if (!sqlite_check_status(status, error))
			goto end;
	}

end:
	if (args_set) va_end(args);
	if (stmt != NULL) sqlite3_finalize(stmt);
	return result;
}

gboolean
get_values (sqlite3_stmt *stmt, va_list args, GError **error)
{
	int column = 0;
	GType type;
	while ((type = va_arg(args, GType)) != 0) switch (type) {
	case G_TYPE_INT:
		*va_arg(args, int*) = sqlite3_column_int(stmt, column++);
		break;
	case G_TYPE_ULONG:
		*va_arg(args, unsigned long*) = sqlite3_column_int(stmt, column++);
		break;
	default:
		/* Unsupported type. */
		g_assert_not_reached();
	}
	return TRUE;
}

void
sqlite_dump (sqlite3_stmt *stmt)
{
#if SQLITE_VERSION_NUMBER >= 3014000
	char *code = sqlite3_expanded_sql(stmt);
	g_print("%s\n", code);
	sqlite3_free(code);
#else
	static gboolean warned = FALSE;
	if (!warned) {
		g_print("# Warning: this version of SQLite cannot dump statements with bound values.\n");
		warned = TRUE;
	}
	g_print("%s\n", sqlite3_sql(stmt));
#endif
}
